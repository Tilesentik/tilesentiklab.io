// TV Controls
var btnMusic = document.querySelector(".btn-tv__music")
var btnDrive = document.querySelector(".btn-tv__cloud")
var btnPackages = document.querySelector(".btn-tv__packages")
var btnMoon = document.querySelector(".btn-tv__moon")
var tv = document.querySelector('.tv')
var tvLink = document.querySelector('.tv-link')

btnMusic.addEventListener('click', function() {
  tv.setAttribute("src", './static/index/img/tv_music.gif')
  tvLink.setAttribute("href", 'https://gitlab.com/stone-services-collection/stone-music')
})

btnDrive.addEventListener('click', function() {
  tv.setAttribute("src", './static/index/img/tv_drive.gif')
  tvLink.setAttribute("href", 'https://gitlab.com/stone-services-collection/stone-drive')
})

btnPackages.addEventListener('click', function() {
  tv.setAttribute("src", './static/index/img/tv_catalog.gif')
  tvLink.setAttribute("href", 'https://gitlab.com/stone-services-collection/stone-catalog')
})

btnPackages.addEventListener('click', function() {
  tv.setAttribute("src", './static/index/img/tv_catalog.gif')
})

btnMoon.addEventListener('click', function() {
  tv.setAttribute("src", './static/index/img/tv_moon.gif')
})


